package com.agreement.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringAgreementTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringAgreementTestApplication.class, args);
	}

}
