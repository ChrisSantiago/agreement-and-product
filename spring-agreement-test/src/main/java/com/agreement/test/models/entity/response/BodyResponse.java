package com.agreement.test.models.entity.response;

public class BodyResponse {

	private Boolean status;

	private String message;

	private Integer code;

	public BodyResponse() {

	}

	public BodyResponse(Boolean status, String message, Integer code) {
		this.status = status;
		this.message = message;
		this.code = code;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	@Override
	public String toString() {
		return "BodyResponse [status=" + status + ", message=" + message + ", code=" + code + "]";
	}

}
