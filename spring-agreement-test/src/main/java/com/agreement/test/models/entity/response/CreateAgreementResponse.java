package com.agreement.test.models.entity.response;

public class CreateAgreementResponse extends BodyResponse {

	private String path;

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}
	
	

	public CreateAgreementResponse(String path, Boolean status, String message, Integer code) {
		this.path = path;
		this.setStatus(status);
		this.setMessage(message);
		this.setCode(code);
	}

	@Override
	public String toString() {
		return "CreateAgreementResponse [path=" + path + "]";
	}

}
