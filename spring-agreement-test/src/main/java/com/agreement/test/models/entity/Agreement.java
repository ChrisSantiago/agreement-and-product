package com.agreement.test.models.entity;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

public class Agreement implements Serializable {

	private static final long serialVersionUID = 1L;

	private String name;

	@NotBlank
	private String signedBy;

	@Valid
	private List<Product> products;

	public Agreement() {
		DateFormat dateFormant = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		this.setName("Agreement" + dateFormant.format(new Date()));
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSignedBy() {
		return signedBy;
	}

	public void setSignedBy(String signedBy) {
		this.signedBy = signedBy;
	}

	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "Agreement [name=" + name + ", signedBy=" + signedBy + ", products=" + products + "]";
	}

}
