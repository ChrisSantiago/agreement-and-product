package com.agreement.test.models.entity;

import java.util.List;

public class AgreementProduct {
	
	private String parentObject;
	
	private List<Product> products;

	public String getParentObject() {
		return parentObject;
	}

	public void setParentObject(String parentObject) {
		this.parentObject = parentObject;
	}

	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}

	@Override
	public String toString() {
		return "AgreementProduct [parentObject=" + parentObject + ", products=" + products + "]";
	}

}
