package com.agreement.test.models.services;

import com.agreement.test.models.entity.Agreement;
import com.agreement.test.models.entity.response.CreateAgreementResponse;

public interface IAgreementService {

	public CreateAgreementResponse createAgreement(Agreement agreement);

	public Object getAgreement(String path);

}
