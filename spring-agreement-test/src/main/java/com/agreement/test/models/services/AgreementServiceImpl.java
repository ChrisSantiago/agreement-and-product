package com.agreement.test.models.services;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.agreement.test.models.entity.Agreement;
import com.agreement.test.models.entity.AgreementProduct;
import com.agreement.test.models.entity.Product;
import com.agreement.test.models.entity.response.BodyResponse;
import com.agreement.test.models.entity.response.CreateAgreementResponse;

@Service
public class AgreementServiceImpl implements IAgreementService {

	private final static String AGREEMENT_PATH = "agreement";

	private final Logger LOG = LoggerFactory.getLogger(getClass());

	@Override
	public CreateAgreementResponse createAgreement(Agreement agreement) {
		AgreementProduct agreementProduct = new AgreementProduct();
		agreementProduct.setParentObject(agreement.getName());
		agreementProduct.setProducts(agreement.getProducts());
		try {
			LOG.info("START TO SAVE INTO FILE");
			File file = new File(AGREEMENT_PATH + "/" + getFileName(agreement.getName()).concat(".txt"));
			BufferedWriter writerFile = new BufferedWriter(new FileWriter(file.getPath()));
			writerFile.write("Agreement name: " + agreement.getName());
			writerFile.newLine();
			writerFile.flush();

			writerFile.write("Signed by: " + agreement.getSignedBy());
			writerFile.newLine();
			writerFile.flush();
			if (agreementProduct.getParentObject() != null && !agreementProduct.getProducts().isEmpty()) {
				writerFile.write("Products: ");
				writerFile.newLine();
				writerFile.flush();
				for (Product product : agreementProduct.getProducts()) {
					writerFile.write(product.getName() + "," + product.getPrice());
					writerFile.newLine();
					writerFile.flush();
				}
			}
			writerFile.close();
			return new CreateAgreementResponse(file.getPath().replaceAll("\\\\", "-").replaceAll(".txt", ""), true,
					"SUCCESS", 0);
		} catch (Exception e) {
			LOG.info("ERROR SAVE INTO FILE: " + e);
		}
		return new CreateAgreementResponse("", true, "INTERNAL ERROR", 2);
	}

	public String getFileName(String name) {
		if (name != null && name.length() > 10) {
			name = name.replaceAll("\\/", "");
			name = name.replace(" ", "_").replaceAll(":", "");
			return name;
		}
		return "NOT_DEFINE_NAME";
	}

	@Override
	public Object getAgreement(String path) {
		Path rootPath = getPath(path);
		File file = rootPath.toFile();
		try {
			if (file.exists() && file.canRead()) {
				List<String> data = FileUtils.readLines(file, StandardCharsets.UTF_8);
				if (!data.isEmpty()) {
					LOG.info("START TO READ FILE");
					String name = data.get(0);
					String signedBy = data.get(1);
					Agreement agreement = new Agreement();
					agreement.setName(name.substring(name.indexOf(":") + 1, name.length()));
					agreement.setSignedBy(signedBy.substring(signedBy.indexOf(":") + 1, signedBy.length()));

					List<Product> products = new ArrayList<>();
					for (int i = 3; i < data.size(); i++) {
						String productString = data.get(i);
						Product product = new Product();
						product.setName(productString.split(",")[0]);
						product.setPrice(Double.parseDouble(productString.split(",")[1]));
						products.add(product);
					}

					agreement.setProducts(products);
					return agreement;
				}
			} else {
				LOG.info("NOT EXIST FILE");
				return new BodyResponse(false, "AGREEMENT NOT FOUND", 4);
			}
		} catch (Exception e) {
			System.out.println(e.getStackTrace());
			LOG.info("ERROR GET AGREEMENT: " + e);
		}
		return new BodyResponse(true, "INTERNAL ERROR", 2);
	}

	public Path getPath(String filename) {
		filename = filename.replaceAll("-", "\\\\").concat(".txt");
		return Paths.get("").resolve(filename).toAbsolutePath();
	}

}
