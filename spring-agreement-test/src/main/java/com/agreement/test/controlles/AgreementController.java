package com.agreement.test.controlles;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.agreement.test.models.entity.Agreement;
import com.agreement.test.models.entity.response.BodyResponse;
import com.agreement.test.models.entity.response.CreateAgreementResponse;
import com.agreement.test.models.services.IAgreementService;

@RestController
@RequestMapping("/agreement")
public class AgreementController {

	@Autowired
	private IAgreementService agreementService;
	
	private final Logger LOG = LoggerFactory.getLogger(getClass());

	@PostMapping("/")
	public CreateAgreementResponse createAgreement(@RequestBody @Valid Agreement request, BindingResult result) {
		LOG.info("REQUEST: " + request.toString());
		if (result.hasErrors()) {
			return new CreateAgreementResponse("",false, "INVALID PARMETERS", 3);
		}
		CreateAgreementResponse response = agreementService.createAgreement(request);
		return response;
	}
	
	@GetMapping("/{path}")
	public Object getAgreement(@PathVariable String path) {
		if(!StringUtils.hasText(path)) {
			return new BodyResponse(false, "INVALID PARMETERS", 3);
		}
		
		Object response = agreementService.getAgreement(path);
		
		return response;
	}

}
